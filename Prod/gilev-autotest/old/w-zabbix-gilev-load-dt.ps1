
$MsSqlUsr=""
$MsSqlPWD=""

#1cSrv server host
$Srvr="localhost"
#1csrv base name
$Ref="gilev-zabbix"
#DB Type
$DBMS="MSSQLServer"
#DB server host
$DBSrvr="localhost"
#DB name
$DB="gilev-zabbix"

$OutDir="C:\Scripts\"
#Uncoment line to run script automatly
#$MsSqlUsr="sa"
#$MsSqlPWD=""
$curdate=Get-Date -Format "yyyy.MM.dd_HH.mm.ss"
While ( !$MsSqlUsr -or !$MsSqlPWD)
{
$MsSqlcred=Get-Credential -Message  "MS SQL user and password" -UserName $MsSqlUsr
$MsSqlUsr=$MsSqlcred.UserName
$MsSqlPWD=$MsSqlcred.Password
if($MsSqlPWD){
$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($MsSqlcred.Password)
$MsSqlPWD = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

}
 
#Write-Host $MsSqlUsr
#Write-Host $MsSqlPwd
#&C:\Program Files (x86)\1cv8\common\1cestart.exe 
$path1c = "C:\Program Files (x86)\1cv8\common\1cestart.exe"
$arg1c = "CREATEINFOBASE Srvr=""$Srvr"";Ref=""$Ref"";DBMS=""$DBMS"";DBSrvr=""$DBSrvr"";DB=""$DB"";DBUID=""$MsSqlUsr"";DBPwd=""$MsSqlPWD"";CrSQLDB=""Y"";SchJobDn=""N"";SQLYOffs=""2000""   /Out ""$OutDir\create_$curdate.log"""


Start-Process -FilePath $path1c  -ArgumentList $arg1c  -NoNewWindow -Wait