[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding("utf-8")

$user="gilev_usr"
Remove-LocalUser -Name  $user   -Confirm:$false
$pass=-join ((45..45) +(48..57)  + (65..90) +(95..95) +(97..122) | Get-Random -Count 18 | % {[char]$_})

New-LocalUser $user -Password (ConvertTo-SecureString -String $pass -AsPlainText -Force)    -FullName "Gilev" -Description "Gilev Test" -AccountNeverExpires   -PasswordNeverExpires
 
UnRegister-ScheduledTask "gilev-zabbix"   -Confirm:$false
$A = New-ScheduledTaskAction -Execute "C:\Program Files (x86)\1cv8\common\1cestart.exe" -Argument 'ENTERPRISE /S"localhost\gilev-zabbix" /N"test"'
$T = New-ScheduledTaskTrigger -Once -At 7am    -RepetitionInterval  (New-TimeSpan -Minutes 30)
$P = New-ScheduledTaskPrincipal $env:computername"\"$user
$S = New-ScheduledTaskSettingsSet
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S
Register-ScheduledTask "gilev-zabbix" -InputObject $D -User $env:computername"\"$user -Password $pass

$accountToAdd="$env:computername\$user"
$sidstr = $null
try {
	$ntprincipal = new-object System.Security.Principal.NTAccount "$accountToAdd"
	$sid = $ntprincipal.Translate([System.Security.Principal.SecurityIdentifier])
	$sidstr = $sid.Value.ToString()
} catch {
	$sidstr = $null
}
Write-Host "Account: $($accountToAdd)" -ForegroundColor DarkCyan
if( [string]::IsNullOrEmpty($sidstr) ) {
	Write-Host "Account not found!" -ForegroundColor Red
	exit -1
}
Write-Host "Account SID: $($sidstr)" -ForegroundColor DarkCyan
$tmp = ""
$tmp = [System.IO.Path]::GetTempFileName()
Write-Host "Export current Local Security Policy" -ForegroundColor DarkCyan
secedit.exe /export /cfg "$($tmp)" 
$c = ""
$c = Get-Content -Path $tmp
$currentSetting = ""
foreach($s in $c) {
	if( $s -like "SeBatchLogonRight*") {
		$x = $s.split("=",[System.StringSplitOptions]::RemoveEmptyEntries)
		$currentSetting = $x[1].Trim()
	}
}
if( $currentSetting -notlike "*$($sidstr)*" ) {
	Write-Host "Modify Setting ""Log on as a Batch Job""" -ForegroundColor DarkCyan
	
	if( [string]::IsNullOrEmpty($currentSetting) ) {
		$currentSetting = "*$($sidstr)"
	} else {
		$currentSetting = "*$($sidstr),$($currentSetting)"
	}
	
	Write-Host "$currentSetting"
	
	$outfile = @"
[Unicode]
Unicode=yes
[Version]
signature="`$CHICAGO`$"
Revision=1
[Privilege Rights]
SeBatchLogonRight = $($currentSetting)
"@
	
	$tmp2 = ""
	$tmp2 = [System.IO.Path]::GetTempFileName()
	
	
	Write-Host "Import new settings to Local Security Policy" -ForegroundColor DarkCyan
	$outfile | Set-Content -Path $tmp2 -Encoding Unicode -Force
	#notepad.exe $tmp2
	Push-Location (Split-Path $tmp2)
	
	try {
		secedit.exe /configure /db "secedit.sdb" /cfg "$($tmp2)" /areas USER_RIGHTS 
		#write-host "secedit.exe /configure /db ""secedit.sdb"" /cfg ""$($tmp2)"" /areas USER_RIGHTS "
	} finally {	
		Pop-Location
	}
} else {
	Write-Host "NO ACTIONS REQUIRED! Account already in ""Log on as a Batch Job""" -ForegroundColor DarkCyan
}
Write-Host "Done." -ForegroundColor DarkCyan



Write-Host "Пароль для входа пользователя $user : $pass" -foregroundcolor green