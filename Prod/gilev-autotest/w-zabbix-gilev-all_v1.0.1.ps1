# Данный скрипт предназначен для автоматического снятия показателей производительности сервера 1С
# Парамет передает значение в элемент данных  gilev на выбраный узел


#
$MsSqlUsr=""
$MsSqlPWD=""

# Имя пользователя который будет создан в системе и от которого будет работать задание в планировщике
$user="gilev_usr"
# Путь к клиенту 1с

$path1c = "C:\Program Files (x86)\1cv8\common\1cestart.exe"
# 1cSrv server host
# Путь к серверу 1С
$Srvr="localhost:1541"
# 1csrv base name
# Название тестовй базы на сервере 1С
$Ref="gilev-zabbix9"
# DB Type
# Тип БД
$DBMS="MSSQLServer"
# DB server host
# Андрес SQL сервера
$DBSrvr="localhost"
# DB name
# имя тестовой базы на SQL сервере и на сервере 1С
$DB="gilev-zabbix9"
# Путь к актуальному архиву со скриптами (самостоятельно не менять!)

$URI="https://gitlab.com/efsol_d5/d54_scripts/-/archive/v0.2/d54_scripts-v0.2.zip"
# Путь к файлу конфигурации zabbbix агента
$ZbxConfigDir="C:\Windows\zabbix-agent\"
# Имя заббикс агента 
$ZbxConfigName="zabbix_agentd.conf"
# Путь к папе где хранятся скрипты

$OutDir="C:\Scripts\"
# Адрес прокси сервера zabbix 
$ZbxProxy="10.15.251.250"


# Uncoment line to run script automatly
# В данной секции заполняем логин и пароль пользователя SQL который может создавать базы.
# Если оставить поля пустыми логин и пароль можно будет ввести в специальном окне, вручную входе выполнения скрипта

#$MsSqlUsr="sa"
#$MsSqlPWD=""


$curdate=Get-Date -Format "yyyy.MM.dd_HH.mm.ss"
While ( !$MsSqlUsr -or !$MsSqlPWD)
{
$MsSqlcred=Get-Credential -Message  "MS SQL user and password" -UserName $MsSqlUsr
$MsSqlUsr=$MsSqlcred.UserName
$MsSqlPWD=$MsSqlcred.Password
if($MsSqlPWD){
$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($MsSqlcred.Password)
$MsSqlPWD = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

}


Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

 
#
if(!$ZbxHostname){
Get-Content ""$ZbxConfigDir\$ZbxConfigName"" | Where-Object {$_ -match "Hostname="} | ForEach-Object {
    $ZbxHostname=$_ -match "Hostname=(?<content>.*)"
    $ZbxHostname=$matches['content']
}
}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
New-Item -ItemType Directory -Force -Path c:\Windows\Temp\d54_scripts
New-Item -ItemType Directory -Force -Path c:\Scripts
Remove-Item c:\Windows\Temp\d54_scripts\* -Recurse
Invoke-WebRequest -URI  $URI -outfile c:\Windows\Temp\d54_scripts.zip
unzip "c:\Windows\Temp\d54_scripts.zip"   "c:\Windows\Temp\d54_scripts\" 
Remove-Item c:\Windows\Temp\d54_scripts.zip
if(Test-Path -Path "C:\Windows\zabbix-agent\gilev.txt")
{
Remove-Item "C:\Windows\zabbix-agent\gilev.txt" -Recurse
}
Add-Content "C:\Windows\zabbix-agent\gilev.txt" "C:\Windows\zabbix-agent\zabbix_sender.exe"
Add-Content "C:\Windows\zabbix-agent\gilev.txt" "gilev"
Add-Content "C:\Windows\zabbix-agent\gilev.txt" $ZbxHostname
Add-Content "C:\Windows\zabbix-agent\gilev.txt" $ZbxProxy


$arg1c = "CREATEINFOBASE Srvr=""$Srvr"";Ref=""$Ref"";DBMS=""$DBMS"";DBSrvr=""$DBSrvr"";DB=""$DB"";DBUID=""$MsSqlUsr"";DBPwd=""$MsSqlPWD"";CrSQLDB=""Y"";SchJobDn=""N"";SQLYOffs=""2000""   /Out ""$OutDir\create_$curdate.log"""


Start-Process -FilePath $path1c  -ArgumentList $arg1c  -NoNewWindow -Wait
 

$arg1c="CONFIG /S ""$Srvr/$Ref""   /RestoreIB""C:\Windows\Temp\d54_scripts\d54_scripts-v0.3\gilev-autotest\zabbix_GILEV_TPC_G1C_83_v.1.5.2.dt""  /Out ""$OutDir\upload_$curdate.log"" "
Start-Process -FilePath $path1c  -ArgumentList $arg1c  -NoNewWindow -Wait



[Console]::OutputEncoding = [System.Text.Encoding]::UTF8


Remove-LocalUser -Name  $user   -Confirm:$false
$pass=-join ((45..45) +(48..57)  + (65..90) +(95..95) +(97..122) | Get-Random -Count 18 | % {[char]$_})

New-LocalUser $user -Password (ConvertTo-SecureString -String $pass -AsPlainText -Force)    -FullName "Gilev" -Description "Gilev Test" -AccountNeverExpires   -PasswordNeverExpires
if(Get-ScheduledTask   | Where-Object { $_.TaskName -eq $Ref } ) 
{
UnRegister-ScheduledTask "$Ref"   -Confirm:$false
}

$A = New-ScheduledTaskAction -Execute $path1c  -Argument "ENTERPRISE /S""$Srvr/$Ref"" /N""test"" "
$T = New-ScheduledTaskTrigger -Once -At 7am    -RepetitionInterval  (New-TimeSpan -Minutes 30)
$P = New-ScheduledTaskPrincipal $env:computername"\"$user
$S = New-ScheduledTaskSettingsSet
$D = New-ScheduledTask -Action $A -Principal $P -Trigger $T -Settings $S
Register-ScheduledTask "$Ref" -InputObject $D -User $env:computername"\"$user -Password $pass

$accountToAdd="$env:computername\$user"


####################################### 
#######################################

######################################
$sidstr = $null
try {
	$ntprincipal = new-object System.Security.Principal.NTAccount "$accountToAdd"
	$sid = $ntprincipal.Translate([System.Security.Principal.SecurityIdentifier])
	$sidstr = $sid.Value.ToString()
} catch {
	$sidstr = $null
}
Write-Host "Account: $($accountToAdd)" -ForegroundColor DarkCyan
if( [string]::IsNullOrEmpty($sidstr) ) {
	Write-Host "Account not found!" -ForegroundColor Red
	exit -1
}
Write-Host "Account SID: $($sidstr)" -ForegroundColor DarkCyan
$tmp = ""
$tmp = [System.IO.Path]::GetTempFileName()
Write-Host "Export current Local Security Policy" -ForegroundColor DarkCyan
secedit.exe /export /cfg "$($tmp)" 
$c = ""
$c = Get-Content -Path $tmp
$currentSetting = ""
foreach($s in $c) {
	if( $s -like "SeBatchLogonRight*") {
		$x = $s.split("=",[System.StringSplitOptions]::RemoveEmptyEntries)
		$currentSetting = $x[1].Trim()
	}
}
if( $currentSetting -notlike "*$($sidstr)*" ) {
	Write-Host "Modify Setting ""Log on as a Batch Job""" -ForegroundColor DarkCyan
	
	if( [string]::IsNullOrEmpty($currentSetting) ) {
		$currentSetting = "*$($sidstr)"
	} else {
		$currentSetting = "*$($sidstr),$($currentSetting)"
	}
	
	Write-Host "$currentSetting"
	
	$outfile = @"
[Unicode]
Unicode=yes
[Version]
signature="`$CHICAGO`$"
Revision=1
[Privilege Rights]
SeBatchLogonRight = $($currentSetting)
"@
	
	$tmp2 = ""
	$tmp2 = [System.IO.Path]::GetTempFileName()
	
	
	Write-Host "Import new settings to Local Security Policy" -ForegroundColor DarkCyan
	$outfile | Set-Content -Path $tmp2 -Encoding Unicode -Force
	#notepad.exe $tmp2
	Push-Location (Split-Path $tmp2)
	
	try {
		secedit.exe /configure /db "secedit.sdb" /cfg "$($tmp2)" /areas USER_RIGHTS 
		#write-host "secedit.exe /configure /db ""secedit.sdb"" /cfg ""$($tmp2)"" /areas USER_RIGHTS "
	} finally {	
		Pop-Location
	}
} else {
	Write-Host "NO ACTIONS REQUIRED! Account already in ""Log on as a Batch Job""" -ForegroundColor DarkCyan
}
Write-Host "Done." -ForegroundColor DarkCyan
####################                                               ######################### 
############################################################################################      
 
Write-Host "Пароль для входа пользователя ""$env:computername\$user"": $pass" -foregroundcolor green