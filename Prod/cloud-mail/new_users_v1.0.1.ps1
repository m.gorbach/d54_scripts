﻿
#Инициализируем переменные

$IsAllGood="n"
$exch_server="exch1"
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$exch_server/PowerShell/ -Authentication Kerberos
Import-PSSession $Session



While ($IsAllGood -eq "n") 
{
    $Domain=""
    $FirstName=""
    $Name=""
    $LastName=""
    $email_name=""
    $prefix=""
    $pass=""

    $prefix=-join ((65..90) + (97..122) | Get-Random -Count 3 | % {[char]$_})
#Write-Host $prefix;

    $pass=-join ((45..45) +(48..57)  + (65..90) +(95..95) +(97..122) | Get-Random -Count 8 | % {[char]$_}) 
#Write-Host $pass

    while (!$Domain) {
    $Domain=read-host "Введите имя домена пользователя"
    }
    

    while (!$FirstName) {
    $FirstName=read-host "Введите имя пользователя на английском языке"
    }
     while (!$LastName) {
    $LastName=read-host "Введите Фамилию пользователя на английском языке"
    }

    while (!$email_name) {
    $email_name=read-host "Введите имя почтового ящика (до знака @)"
    }



Write-Host ""
Write-Host "" 
Write-Host "Будет создан пользователь со следующими параметрами" -foregroundcolor green
Write-Host "====================================================" -foregroundcolor green
Write-Host "Имя домена, где будет создан пользователь: $Domain" -foregroundcolor green
Write-Host "Имя пользователя на английском языке: $FirstName" -foregroundcolor green
Write-Host "Имя пользователя на английском языке: $LastName" -foregroundcolor green
Write-Host "Имя почтового ящика: $email_name@$Domain" -foregroundcolor green
Write-Host "Пароль для входа: $pass" -foregroundcolor green
Write-Host ""
Write-Host ""

#$SecurePass=ConvertTo-SecureString -String $pass  
$IsAllGood="y"
$IsAllGood=read-host "Все правильно? Создать пользователя? (y/n). По умолчанию Да (y)"
}

New-Mailbox -Name "$FirstName $LastName" -UserPrincipalName $email_name@$Domain -SamAccountName "$email_name$prefix" -FirstName $FirstName -LastName  $Lastname -Password (ConvertTo-SecureString -String $pass -AsPlainText -Force)  -ResetPasswordOnNextLogon $false -AddressBookPolicy $Domain -OrganizationalUnit  "ad.cloudmail.local/$Domain" -Database $Domain 
Set-Mailbox "$FirstName $LastName" -CustomAttribute1 $Domain 


Remove-PSSession $Session