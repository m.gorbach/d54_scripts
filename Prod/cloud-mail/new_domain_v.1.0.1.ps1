
$NewDomain=""

while (!$NewDomain) {
$NewDomain=read-host "������� ��� ������ ������ "
}

$exch_server="exch1"


If ($exch_server -eq  "exch1") {
$exch_copy="exch2"
}
If ($exch_server -eq "exch2") {
$exch_copy="exch1"
}


#��������� �������
Set-ADForest -Identity ad.cloudmail.local -UPNSuffixes @{add="$NewDomain"}

# ��������� OU
New-ADOrganizationalUnit -Name $NewDomain

#������������ � ������� Powershell � ������� Exchange
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$exch_server/PowerShell/ -Authentication Kerberos
Import-PSSession $Session

#������� ����� ���� ������ ��� �������
New-MailboxDatabase  -Server $exch_server  -Name "$NewDomain" -EdbFilePath "M:\Database\$NewDomain\$NewDomain.edb" -LogFolderPath "M:\Database\$NewDomain\"
#���������� ����
Mount-Database  -Identity "$NewDomain"
#������ ����������
#Add-MailboxDatabaseCopy -Identity "$NewDomain" -MailboxServer $exch_copy


# ��������� ����������� �����
New-AcceptedDomain -Name $NewDomain -DomainName $NewDomain -DomainType:Authoritative

#  Create Global Address List 
New-GlobalAddressList -Name "$NewDomain - GAL" -ConditionalCustomAttribute1 "$NewDomain" -IncludedRecipients MailboxUsers -RecipientContainer "ad.cloudmail.local/$NewDomain"

#Create All Rooms Address List
New-AddressList -Name "$NewDomain � All Rooms" -RecipientFilter "(CustomAttribute1 -eq '$NewDomain') -and (RecipientDisplayType -eq 'ConferenceRoomMailbox')" -RecipientContainer "ad.cloudmail.local/$NewDomain"

# Create All Users Address List
New-AddressList -Name "$NewDomain � All Users" -RecipientFilter "(CustomAttribute1 -eq '$NewDomain')  -and (ObjectClass -eq 'User')" -RecipientContainer "ad.cloudmail.local/$NewDomain"
# 8. Create All Contacts Address List

New-AddressList -Name "$NewDomain - All Contacts" -RecipientFilter "(CustomAttribute1 -eq '$NewDomain') -and (ObjectClass -eq 'Contact')"  -RecipientContainer "ad.cloudmail.local/$NewDomain"
# 9. Create All Groups Address List

New-AddressList -Name "$NewDomain � All Groups" -RecipientFilter "(CustomAttribute1 -eq '$NewDomain') -and (ObjectClass -eq 'Group')" -RecipientContainer "ad.cloudmail.local/$NewDomain"

# 10. Create Offline Address Book

New-OfflineAddressBook -Name "$NewDomain" -AddressLists "$NewDomain - GAL"

#11. Create Email Address Policy

New-EmailAddressPolicy -Name "$NewDomain"  -RecipientContainer "ad.cloudmail.local/$NewDomain" -IncludedRecipients "AllRecipients" -ConditionalCustomAttribute1 "$NewDomain" -EnabledEmailAddressTemplates "SMTP:%m@$NewDomain","smtp:%g.%s@$NewDomain"


#  Create Address Book Policy
New-AddressBookPolicy -Name "$NewDomain" -AddressLists "$NewDomain � All Users" , "$NewDomain - All Contacts", "$NewDomain � All Groups" -GlobalAddressList "$NewDomain - GAL" -OfflineAddressBook "$NewDomain" -RoomList "$NewDomain � All Rooms"




Remove-PSSession $Session

