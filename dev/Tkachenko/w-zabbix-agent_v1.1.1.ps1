
$string="10.15.251.250"
$FileName = "c:\Windows\zabbix-agent\zabbix_agentd.conf"
$ZabbixDir="C:\Windows\zabbix-agent"




Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}



New-Item -ItemType Directory -Force -Path c:\Windows\Temp\zabbix-agent

Remove-Item c:\Windows\Temp\zabbix-agent\* -Recurse
Invoke-WebRequest -URI https://www.zabbix.com/downloads/4.0.11/zabbix_agents-4.0.11-win-amd64-openssl.zip  -outfile c:\Windows\Temp\zabbix-agent.zip
unzip "c:\Windows\Temp\zabbix-agent.zip"   "C:\Windows\temp\zabbix-agent\" 


Remove-Item c:\Windows\Temp\zabbix-agent.zip

New-Item -ItemType Directory -Force -Path c:\Windows\zabbix-agent
Copy-Item "c:\Windows\Temp\zabbix-agent\bin\*" -Destination "c:\Windows\zabbix-agent\" -Recurse -Force 
Copy-Item "c:\Windows\Temp\zabbix-agent\conf\*" -Destination "c:\Windows\zabbix-agent\" -Recurse -Force 

$FileExists = Test-Path $FileName

If ($FileExists ) {
    $FileName = "c:\Windows\zabbix-agent\zabbix_agentd.conf"
}
 else{
    Write-Host "Config file not exist"
    Break
}


$FileOriginal = Get-Content $FileName
$hostname = $env:computername

[String]$hostname = $hostname

[String[]]$FileModified = @()

Foreach ($Line in $FileOriginal){
if ($Line -match "Server=127.0.0.1") {
$FileModified += $Line.Replace($Line, "Server=$string")
}
elseif ($Line -match "Hostname=Windows host") {
$FileModified += $Line.Replace($Line, "Hostname=$hostname")
}
 
elseif ($Line -match "ServerActive=127.0.0.1") {
$FileModified += $Line.Replace($Line, "ServerActive=$string")
}
elseif ($Line -match "LogFile=c") {
$FileModified += $Line.Replace($Line, "LogFile=$ZabbixDir\zabbix_agentd.log")
}
else {
$FileModified += $Line
}
}
Set-Content $fileName $FileModified -Force



Start-Service 'Zabbix Agent'
Remove-Item c:\Windows\Temp\zabbix-agent\* -Recurse




c:\Windows\zabbix-agent\zabbix_agentd.exe --config c:\Windows\zabbix-agent\zabbix_agentd.conf --install


New-NetFirewallRule -Name "Zabbix_IN" -DisplayName "Zabbix_IN" -Enabled:True -Profile Any -Direction Inbound -Action Allow -Protocol TCP -LocalPort 10050
New-NetFirewallRule -Name "Zabbix_OUT" -DisplayName "Zabbix_OUT" -Enabled:True -Profile Any -Direction Outbound -Action Allow -Protocol TCP -LocalPort 10050
Start-Service 'Zabbix Agent'
Get-Service "zabbix agent" | Format-List -Property Name,Status