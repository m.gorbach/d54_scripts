
$string="10.15.251.250"
$FileName = "c:\Windows\zabbix-agent\zabbix_agentd.conf"





Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}



New-Item -ItemType Directory -Force -Path c:\Windows\Temp\zabbix-agent
Remove-Item c:\Windows\Temp\zabbix-agent\* -Recurse
Invoke-WebRequest -URI https://www.zabbix.com/downloads/4.0.11/zabbix_agents-4.0.11-win-amd64-openssl.zip  -outfile c:\Windows\Temp\zabbix-agent.zip
unzip "c:\Windows\Temp\zabbix-agent.zip"   "C:\Windows\temp\zabbix-agent\" 


Remove-Item c:\Windows\Temp\zabbix-agent.zip

Stop-Service 'Zabbix Agent'

$FileExists = Test-Path $FileName

If ($FileExists ) {
    $FileName = "c:\Windows\zabbix-agent\zabbix_agentd.conf"
}
else{
$FileName = "c:\Windows\zabbix-agent\zabbix_agentd.win.conf"
$FileExists = Test-Path $FileName
    if ($FileExists) {
    $FileName = "c:\Windows\zabbix-agent\zabbix_agentd.win.conf"
    }
    else{
    Write-Host "Config file not exist"
    return
    }
}

$FileOriginal = Get-Content $FileName

[String[]]$FileModified = @()

Foreach ($Line in $FileOriginal){
if ($Line -match "Server=") {
$FileModified += $Line.Replace($Line, "Server=$string")
} 
elseif ($Line -match "ServerActive=") {
$FileModified += $Line.Replace($Line, "ServerActive=$string")
}
elseif ($Line -match "#LogFile=") {
$FileModified += $Line.Replace($Line, "LogFile=c:\windows\zabbix-agent\zabbix_agentd.log")
}
else {
$FileModified += $Line
}
}
Set-Content $fileName $FileModified -Force


Copy-Item "c:\Windows\Temp\zabbix-agent\bin\*" -Destination "c:\Windows\zabbix-agent\" -Recurse -Force 
Start-Service 'Zabbix Agent'
Remove-Item c:\Windows\Temp\zabbix-agent\* -Recurse
Get-Service "zabbix agent" | Format-List -Property Name,Status